extern crate rnm;

use rnm::file_list::FileList;
use rnm::options::Options;
use std::io;
use std::io::Write;

#[cfg_attr(test, allow(dead_code))]
fn main() {
    let (options, directories) = Options::new();

    for ref dir in directories {
        if let Err(e) = FileList::new(dir, &options).and_then(|fl| fl.rename_files()) {
            writeln!(
                &mut io::stderr(),
                "Failed to rename files in {}: {}",
                dir.to_str().unwrap(),
                e.to_string(),
            )
            .unwrap();
            break;
        }
    }
}
