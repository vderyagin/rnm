use std::num::ParseIntError;
use std::ops::Index;
use std::path::{Path, PathBuf};
use std::{fmt, fs, io};

#[derive(PartialEq, Eq, Debug, Hash, Clone)]
pub struct File(PathBuf);

impl File {
    pub fn new(path: &Path) -> Result<Self, String> {
        if path.is_absolute() {
            Ok(File(path.to_path_buf()))
        } else {
            Err(format!("{:?} is not an absolute path", path))
        }
    }

    pub fn basename(&self) -> &str {
        let &File(ref path) = self;

        path.file_stem().and_then(|stem| stem.to_str()).unwrap()
    }

    pub fn common_prefix<'a>(&self, mut prefix: &'a str) -> &'a str {
        let basename = self.basename();

        if basename == prefix {
            // don't leave empty string after stripping prefix
            if let Some((last_char_index, _)) = prefix.char_indices().last() {
                return prefix.index(..last_char_index);
            }
        }

        loop {
            if basename.starts_with(prefix) {
                break;
            } else {
                prefix = if let Some((last_char_index, _)) = prefix.char_indices().last() {
                    prefix.index(..last_char_index)
                } else {
                    ""
                }
            }
        }

        prefix
    }

    pub fn common_suffix<'a>(&self, mut suffix: &'a str) -> &'a str {
        let basename = self.basename();

        if basename == suffix {
            // don't leave empty string after stripping suffix
            if let Some((first_char_index, _)) = suffix.char_indices().nth(1) {
                return suffix.index(first_char_index..);
            }
        }

        loop {
            if basename.ends_with(suffix) {
                break;
            } else {
                suffix = if let Some((first_char_index, _)) = suffix.char_indices().nth(1) {
                    suffix.index(first_char_index..)
                } else {
                    ""
                }
            }
        }

        suffix
    }

    fn clean_basename(&self, prefix: &str, suffix: &str) -> &str {
        let basename = self.basename();
        let suffix_start = basename.len() - suffix.len();
        let prefix_start = prefix.len();

        basename.index(prefix_start..suffix_start)
    }

    fn pad_with_zeros(&self, mut s: String, len: usize) -> String {
        while s.len() < len {
            s.insert(0, '0');
        }
        s
    }

    pub fn new_name(
        &self,
        prefix: &str,
        suffix: &str,
        num_length: usize,
        num_override: Option<usize>,
    ) -> Option<PathBuf> {
        let &File(ref path) = self;

        let mut clean_basename = if let Some(number) = num_override {
            number
        } else if let Ok(number) = self.number(prefix, suffix) {
            number
        } else {
            return None;
        }
        .to_string();

        clean_basename = self.pad_with_zeros(clean_basename, num_length);

        let mut new_path_buf = path.with_file_name(clean_basename);

        if let Some(ext) = path.extension() {
            new_path_buf = new_path_buf.with_extension(ext)
        }

        Some(new_path_buf)
    }

    pub fn number(&self, prefix: &str, suffix: &str) -> Result<usize, ParseIntError> {
        self.clean_basename(prefix, suffix).parse::<usize>()
    }

    pub fn rename(&self, new: &Path) -> io::Result<()> {
        let &File(ref original) = self;
        fs::rename(original, new)?;
        println!("mv {:?} -> {:?}", original, new);
        Ok(())
    }

    pub fn as_path(&self) -> &Path {
        let &File(ref path) = self;
        path.as_path()
    }

    pub fn to_path_buf(&self) -> PathBuf {
        let &File(ref path) = self;
        path.to_path_buf()
    }
}

impl fmt::Display for File {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let &File(ref path) = self;
        write!(f, "{}", path.display())
    }
}

#[cfg(test)]
mod tests {
    use super::File;
    use std::path::Path;

    fn file(path: &str) -> File {
        File::new(Path::new(path)).unwrap()
    }

    mod constructor {
        use super::super::File;
        use std::path::Path;

        #[test]
        fn only_accepts_absolute_paths() {
            assert!(File::new(Path::new("/absolute/path")).is_ok());
            assert!(File::new(Path::new("relative/path")).is_err());
        }
    }

    mod basename {
        use super::file;

        #[test]
        fn plain() {
            assert_eq!(file("/bar").basename(), "bar");
        }

        #[test]
        fn nested() {
            assert_eq!(file("/foo/bar").basename(), "bar");
        }

        #[test]
        fn nested_with_extension() {
            assert_eq!(file("/foo/bar.baz").basename(), "bar");
        }
    }

    mod common_prefix_suffix {
        use super::file;

        #[test]
        fn empty() {
            assert_eq!(file("/12345").common_prefix("abc"), "");
            assert_eq!(file("/12345").common_suffix("abc"), "");
        }

        #[test]
        fn nonempty() {
            assert_eq!(file("/ab123").common_prefix("abc"), "ab");
            assert_eq!(file("/123bc").common_suffix("abc"), "bc");
        }

        #[test]
        fn nonempty_with_extension() {
            assert_eq!(file("/ab123.txt").common_prefix("abc"), "ab");
            assert_eq!(file("/123bc.txt").common_suffix("abc"), "bc");
        }

        #[test]
        fn does_not_contain_full_name() {
            assert_eq!(file("/2foo1").common_prefix("2foo1"), "2foo");
            assert_eq!(file("/2foo1").common_suffix("2foo1"), "foo1");

            assert_eq!(file("/2foo1.txt").common_prefix("2foo1"), "2foo");
            assert_eq!(file("/2foo1.jpg").common_suffix("2foo1"), "foo1");
        }
    }

    mod new_name {
        use super::file;
        use std::path::PathBuf;

        #[test]
        fn same_as_old() {
            assert_eq!(
                file("/123.jpg").new_name("", "", 3, None).unwrap(),
                PathBuf::from("/123.jpg")
            );
        }

        #[test]
        fn with_prefix_suffix_and_extension() {
            assert_eq!(
                file("/foo/bar012baz.quux")
                    .new_name("bar", "baz", 2, None)
                    .unwrap(),
                PathBuf::from("/foo/12.quux")
            );
        }

        #[test]
        fn allows_to_specify_number_length() {
            assert_eq!(
                file("/foo/1.txt").new_name("", "", 3, None).unwrap(),
                PathBuf::from("/foo/001.txt")
            );
        }

        #[test]
        fn allows_to_override_number() {
            assert_eq!(
                file("/foo/1.txt").new_name("", "", 2, Some(8)).unwrap(),
                PathBuf::from("/foo/08.txt")
            );
        }
    }

    mod number {
        use super::file;

        #[test]
        fn panics_if_there_are_nondigits_left_after_cleaning() {
            assert!(file("/foo/a123.jpg").number("", "").is_err());
        }

        #[test]
        fn extracts_number_from_simple_names() {
            assert_eq!(file("/123").number("", ""), Ok(123));
            assert_eq!(file("/foo/234").number("", ""), Ok(234));
        }

        #[test]
        fn works_with_complex_names() {
            assert_eq!(file("/foo/bar/baz0032qux.txt").number("baz", "qux"), Ok(32));
        }
    }

    mod clean_basename {
        use super::file;

        #[test]
        fn does_not_strip_prefix_repeatedly() {
            assert_eq!(file("/11123").clean_basename("1", ""), "1123");
        }

        #[test]
        fn does_not_strip_suffix_repeatedly() {
            assert_eq!(file("/12333").clean_basename("", "3"), "1233");
        }
    }
}
