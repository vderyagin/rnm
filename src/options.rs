use clap::{App, Arg};
use std::collections::HashSet;
use std::io::Write;
use std::path::PathBuf;
use std::{env, io, process};

pub struct Options {
    pub dry_run: bool,
    pub quiet: bool,
    pub renumber: bool,

    pub only: HashSet<PathBuf>,
    pub except: HashSet<PathBuf>,
}

impl Options {
    pub fn new() -> (Self, HashSet<PathBuf>) {
        let matches = App::new("rnm")
            .version("0.3.1")
            .author("Victor Deryagin <vderyagin@gmail.com>")
            .about("Renames numbered files")
            .arg(
                Arg::with_name("DIR")
                    .multiple(true)
                    .use_delimiter(false)
                    .help("Directories to rename files in")
                    .validator(validate_dir),
            )
            .arg(
                Arg::with_name("renumber")
                    .long("renumber")
                    .help("Renumber files starting from 1"),
            )
            .arg(
                Arg::with_name("quiet")
                    .short("q")
                    .long("quiet")
                    .help("Don't log renames"),
            )
            .arg(
                Arg::with_name("dry-run")
                    .long("dry-run")
                    .help("Show how files would be renamed without actually renaming any"),
            )
            .arg(
                Arg::with_name("INCLUDED_FILE")
                    .long("only")
                    .takes_value(true)
                    .multiple(true)
                    .use_delimiter(false)
                    .help("Limit renaming to specified files")
                    .validator(validate_file),
            )
            .arg(
                Arg::with_name("EXCLUDED_FILE")
                    .long("except")
                    .takes_value(true)
                    .multiple(true)
                    .use_delimiter(false)
                    .help("Exclude specified files from renaming")
                    .validator(validate_file),
            )
            .get_matches();

        if matches.is_present("dry-run") && matches.is_present("quiet") {
            die("--quiet and --dry-run flags can not be used at the same time");
        }

        if matches.is_present("INCLUDED_FILE") && matches.is_present("EXCLUDED_FILE") {
            die("--only and --except options can not be used at the same time");
        }

        let only = files_from(matches.values_of("INCLUDED_FILE"), vec![]);
        let except = files_from(matches.values_of("EXCLUDED_FILE"), vec![]);
        let dirs = files_from(matches.values_of("DIR"), vec!["."]);

        let opts = Options {
            dry_run: matches.is_present("dry-run"),
            quiet: matches.is_present("quiet"),
            renumber: matches.is_present("renumber"),
            only: only,
            except: except,
        };

        (opts, dirs)
    }

    pub fn is_included(&self, file: PathBuf) -> bool {
        if self.only.is_empty() && self.except.is_empty() {
            true
        } else if !self.only.is_empty() {
            self.only.contains(&file)
        } else {
            !self.except.contains(&file)
        }
    }
}

fn files_from<'a, T: Iterator<Item = &'a str>>(
    res: Option<T>,
    default: Vec<&str>,
) -> HashSet<PathBuf> {
    res.map_or(default, |coll| coll.collect())
        .iter()
        .map(|s| s.to_string())
        .map(PathBuf::from)
        .map(absolute_path)
        .collect()
}

fn absolute_path(path: PathBuf) -> PathBuf {
    if path.is_absolute() {
        path
    } else {
        current_dir().join(path)
    }
    .canonicalize()
    .unwrap()
}

fn current_dir() -> PathBuf {
    env::current_dir().expect("Invalid working directory")
}

fn validate_dir(dir_name: String) -> Result<(), String> {
    if PathBuf::from(&dir_name).is_dir() {
        Ok(())
    } else {
        Err(format!("no directory found at '{}'", dir_name))
    }
}

fn validate_file(file_name: String) -> Result<(), String> {
    if PathBuf::from(&file_name).is_file() {
        Ok(())
    } else {
        Err(format!("no regular file found at '{}'", file_name))
    }
}

fn die(msg: &str) -> ! {
    writeln!(&mut io::stderr(), "{}", msg).unwrap();
    process::exit(1);
}
