use std::path::{Path, PathBuf};
use std::{fs, io};

use super::file::File;
use super::options::Options;
use super::rename;

pub struct FileList<'a> {
    directory: PathBuf,
    options: &'a Options,
}

impl<'a> FileList<'a> {
    pub fn new(dir: &Path, opts: &'a Options) -> io::Result<Self> {
        if dir.is_absolute() {
            Ok(FileList {
                directory: dir.to_path_buf(),
                options: opts,
            })
        } else {
            Err(io::Error::new(
                io::ErrorKind::Other,
                format!("{:?} is not an absolute path", dir),
            ))
        }
    }

    fn common_prefix_suffix(&self, files: &[File]) -> (String, String) {
        let first_file_name = files[0].basename();

        let prefix = files
            .iter()
            .fold(first_file_name, |acc, item| item.common_prefix(acc));

        let suffix = files
            .iter()
            .fold(first_file_name, |acc, item| item.common_suffix(acc));

        (prefix.to_string(), suffix.to_string())
    }

    fn validate_names(&self, files: &[File], prefix: &str, suffix: &str) -> io::Result<()> {
        for file in files {
            if let Err(_) = file.number(prefix, suffix) {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!("filename must contain only digits: {}", file),
                ));
            }
        }

        Ok(())
    }

    fn new_file_names(
        &self,
        files: &mut [File],
        renumber: bool,
    ) -> io::Result<Vec<(PathBuf, PathBuf)>> {
        let (prefix, suffix) = self.common_prefix_suffix(files);

        self.validate_names(&files, &prefix, &suffix)?;

        let mut renames = Vec::with_capacity(files.len());
        let mut num: Option<usize>;
        let max_number_length: usize;

        if renumber {
            num = Some(1);
            max_number_length = files.len().to_string().len();
            files.sort_by(|a, b| {
                a.number(&prefix, &suffix)
                    .unwrap()
                    .cmp(&b.number(&prefix, &suffix).unwrap())
            });
        } else {
            num = None;
            max_number_length = self.max_number_length(files, &prefix, &suffix);
        }

        for file in files {
            if let Some(new) = file.new_name(&prefix, &suffix, max_number_length, num) {
                if new != file.to_path_buf() {
                    renames.push((file.to_path_buf(), new));
                }
            }

            num = num.map(|n| n + 1);
        }

        Ok(renames)
    }

    fn max_number_length(&self, files: &[File], prefix: &str, suffix: &str) -> usize {
        files
            .iter()
            .map(|f| f.number(prefix, suffix).unwrap())
            .max()
            .unwrap()
            .to_string()
            .len()
    }

    fn files(&self) -> io::Result<Vec<File>> {
        let dir = &self.directory;
        let files = fs::read_dir(dir)?;
        Ok(files
            .map(|entry| entry.unwrap())
            .filter(|entry| entry.file_type().unwrap().is_file())
            .filter(|entry| self.options.is_included(entry.path()))
            .map(|entry| File::new(&entry.path()).unwrap())
            .collect())
    }

    pub fn rename_files(&self) -> io::Result<()> {
        let mut files = self.files()?;

        if !files.is_empty() {
            let renames = self.new_file_names(&mut files, self.options.renumber)?;
            rename::do_renames(&renames, self.options)?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    mod constructor {
        use super::super::super::options::Options;
        use super::super::FileList;
        use std::collections::HashSet;
        use std::path::Path;

        #[test]
        fn accepts_only_absolute_paths() {
            let opts = Options {
                only: HashSet::new(),
                except: HashSet::new(),
                dry_run: false,
                renumber: false,
                quiet: false,
            };

            assert!(FileList::new(Path::new("relative/path"), &opts).is_err());
            assert!(FileList::new(Path::new("/absolute/path"), &opts).is_ok());
        }
    }
}
