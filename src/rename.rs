use std::path::{Path, PathBuf};
use std::{env, fs, io};

use base64;
use rand::prelude::*;

use super::options::Options;

fn conflict_check(renames: &[(PathBuf, PathBuf)]) -> Result<(), String> {
    let mut new_names = renames.iter().map(|ref tpl| &tpl.1).collect::<Vec<_>>();

    new_names.sort();
    new_names.dedup();

    if new_names.len() == renames.len() {
        Ok(())
    } else {
        Err("Trying to rename more then one file to the same name".to_string())
    }
}

pub fn do_renames(renames: &[(PathBuf, PathBuf)], options: &Options) -> io::Result<()> {
    if let Err(err) = conflict_check(renames) {
        return Err(io::Error::new(io::ErrorKind::InvalidData, err));
    }

    if options.dry_run {
        for &(ref original, ref new) in renames {
            log_rename(original, new);
        }

        return Ok(());
    }

    let mut renames = renames.to_vec();
    let mut deferred_renames = Vec::with_capacity(renames.len());
    let mut progress = false; // was progress made in current iteration?

    loop {
        while let Some((original, new)) = renames.pop() {
            if fs::metadata(&new).is_ok() {
                deferred_renames.push((original, new));
            } else {
                fs::rename(&original, &new)?;
                progress = true;

                if !options.quiet {
                    log_rename(&original, &new);
                }
            }
        }

        if deferred_renames.is_empty() {
            // renaming is over
            break;
        }

        if !progress {
            // we got ourselves a cycle
            if let Some((original, new)) = deferred_renames.pop() {
                let temp = temp_file(new.as_path());
                renames.push((original, temp.clone()));
                deferred_renames.push((temp, new));
            }
        }

        if renames.is_empty() {
            renames = deferred_renames;
            deferred_renames = Vec::with_capacity(renames.len());
            progress = false;
        }
    }

    Ok(())
}

fn temp_file(original: &Path) -> PathBuf {
    let mut rand_data = [0u8; 16];
    rand::thread_rng().fill_bytes(&mut rand_data);

    original
        .to_path_buf()
        .with_file_name(base64::encode(&rand_data))
}

fn current_dir() -> PathBuf {
    env::current_dir().expect("Invalid working directory")
}

fn log_rename(original: &Path, new: &Path) {
    let wd = current_dir();

    let original_rel = if let Ok(rel_path) = original.strip_prefix(&wd) {
        rel_path
    } else {
        original
    }
    .to_str()
    .unwrap();

    let new_rel = if let Ok(rel_path) = new.strip_prefix(&wd) {
        rel_path
    } else {
        new
    }
    .to_str()
    .unwrap();

    println!("{} → {}", original_rel, new_rel);
}

#[cfg(test)]
mod tests {
    mod conflict_check {
        use super::super::conflict_check;
        use std::path::PathBuf;

        #[test]
        fn detects_attempts_to_rename_multiple_files_to_same_name() {
            let renames = &[
                (PathBuf::from("foo"), PathBuf::from("bar")),
                (PathBuf::from("qux"), PathBuf::from("bar")),
            ];
            assert!(conflict_check(renames).is_err());
        }

        #[test]
        fn accepts_renaming_files_to_all_different_names() {
            let renames = &[
                (PathBuf::from("foo"), PathBuf::from("bar")),
                (PathBuf::from("qux"), PathBuf::from("corge")),
            ];
            assert!(conflict_check(renames).is_ok());
        }
    }
}
