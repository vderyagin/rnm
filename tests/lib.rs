extern crate rnm;
extern crate tempdir;

use rnm::file_list::FileList;
use rnm::options::Options;
use rnm::rename::do_renames;
use std::collections::HashSet;
use std::fs;
use std::io::{Read, Write};
use std::path::Path;
use tempdir::TempDir;

fn in_temp_dir(action: &Fn(&Path) -> ()) {
    action(TempDir::new("rnm_tests").unwrap().path());
}

fn create_file(path: &Path) -> fs::File {
    fs::File::create(path).unwrap()
}

fn create_file_with_content(path: &Path, content: &[u8]) -> fs::File {
    let mut file = fs::File::create(path).unwrap();
    file.write_all(content).unwrap();
    file
}

fn file_content(path: &Path) -> String {
    let mut content = String::new();
    fs::File::open(path)
        .unwrap()
        .read_to_string(&mut content)
        .unwrap();
    content
}

fn exists(path: &Path) -> bool {
    fs::metadata(path).is_ok()
}

#[test]
fn does_not_rename_if_names_already_fine() {
    in_temp_dir(&|dir| {
        let files = (1..5)
            .map(|n| dir.join(format!("{}", n)))
            .collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(exists(file));
        }
    })
}

#[test]
fn truncates_leading_zeros() {
    in_temp_dir(&|dir| {
        let files = (1..5)
            .map(|n| dir.join(format!("00{}", n)))
            .collect::<Vec<_>>();

        let files_proper = (1..5).map(|n| dir.join(n.to_string())).collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(!exists(file));
        }

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn removes_prefix_and_suffix() {
    in_temp_dir(&|dir| {
        let files = (1..5)
            .map(|n| dir.join(format!("foo000{}bar.baz", n)))
            .collect::<Vec<_>>();

        let files_proper = (1..5)
            .map(|n| dir.join(format!("{}.baz", n)))
            .collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(!exists(file));
        }

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn renumbers_files_when_told_so() {
    in_temp_dir(&|dir| {
        let files = (8..12).map(|n| dir.join(n.to_string())).collect::<Vec<_>>();

        let files_proper = (1..5).map(|n| dir.join(n.to_string())).collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: true,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(!exists(file));
        }

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn does_not_strip_too_many_zeros() {
    in_temp_dir(&|dir| {
        let files = vec!["0001", "0002", "0003", "0030"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<Vec<_>>();

        let files_proper = vec!["01", "02", "03", "30"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(!exists(file));
        }

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn works_fine_with_multibyte_characters() {
    in_temp_dir(&|dir| {
        let files = (1..5)
            .map(|n| dir.join(format!("→→{}←←.©™", n)))
            .collect::<Vec<_>>();

        let files_proper = (1..5)
            .map(|n| dir.join(format!("{}.©™", n)))
            .collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files {
            assert!(!exists(file));
        }

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn deals_with_naming_conflicts() {
    in_temp_dir(&|dir| {
        let files = (2..10).map(|n| dir.join(n.to_string())).collect::<Vec<_>>();

        let files_proper = (1..9).map(|n| dir.join(n.to_string())).collect::<Vec<_>>();

        for file in &files {
            create_file(file);
        }

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: true,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        assert!(!exists(&files.last().unwrap()));

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn allows_to_exclude_files_from_renaming() {
    in_temp_dir(&|dir| {
        let files = vec!["0001", "0002", "0003", "0030", "foo.jpg", "bar.jpg"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<HashSet<_>>();

        let files_proper = vec!["01", "02", "03", "30", "foo.jpg", "bar.jpg"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<HashSet<_>>();

        for file in &files {
            create_file(file);
        }

        let mut except = HashSet::new();
        except.insert(dir.join("foo.jpg"));
        except.insert(dir.join("bar.jpg"));

        let ref opts = Options {
            only: HashSet::new(),
            except: except,
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn allows_to_limit_renaming_to_specified_files() {
    in_temp_dir(&|dir| {
        let files = vec!["0001.jpg", "0002.jpg", "0030.jpg", "foo", "bar"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<HashSet<_>>();

        let files_proper = vec!["01.jpg", "02.jpg", "30.jpg", "foo", "bar"]
            .iter()
            .map(|n| dir.join(n))
            .collect::<HashSet<_>>();

        for file in &files {
            create_file(file);
        }

        let mut only = HashSet::new();
        only.insert(dir.join("0001.jpg"));
        only.insert(dir.join("0002.jpg"));
        only.insert(dir.join("0030.jpg"));

        let ref opts = Options {
            only: only,
            except: HashSet::new(),
            dry_run: false,
            renumber: false,
            quiet: true,
        };

        FileList::new(dir, opts).unwrap().rename_files().unwrap();

        for file in &files_proper {
            assert!(exists(file));
        }
    })
}

#[test]
fn resolves_renaming_cycles() {
    in_temp_dir(&|dir| {
        let a = dir.join("a");
        let b = dir.join("b");
        let c = dir.join("c");

        create_file_with_content(&a, b"a");
        create_file_with_content(&b, b"b");
        create_file_with_content(&c, b"c");

        let renames = &[
            (a.clone(), b.clone()),
            (b.clone(), c.clone()),
            (c.clone(), a.clone()),
        ];

        let ref opts = Options {
            only: HashSet::new(),
            except: HashSet::new(),
            dry_run: false,
            renumber: true,
            quiet: true,
        };

        assert!(do_renames(renames, opts).is_ok());
        assert!(exists(&a) && exists(&b) && exists(&c));

        assert_eq!(&file_content(&a), "c");
        assert_eq!(&file_content(&b), "a");
        assert_eq!(&file_content(&c), "b");
    })
}
